import { Component, OnInit } from '@angular/core';
import { StoreModel } from '../../models/store.model';
import { Category } from '../../constants/category.constant';

@Component({
  selector: 'nc-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.scss']
})
export class StoresComponent implements OnInit {

  withEmpty = true;
  stores: Array<StoreModel> = [];
  categories: Array<string> = [];

  constructor() { }

  ngOnInit(): void {
    this.stores = this.getData();
    this.categories = this.getAllCategories();
  }

  /**
   * Récupérer toutes les catégories possibles
   */
  private getAllCategories(): Array<string> {
    return Object.keys(Category).sort();
  }

  /**
   * Filtrer les paniers des commerces par catégorie
   * @param category  catégorie de panier
   */
  getStoresByCategory(category: string): Array<StoreModel> {
    return this.stores.filter(s => s.category === category);
  }

  /**
   * Filtrer les paniers vides ou non
   * @param value Filtre sur les paniers vides
   */
  filterEmpty(value: boolean): void {
    this.withEmpty = value;
  }

  /**
   * Mock pour récupérer toutes les données sur les commerces
   */
  /* tslint:disable:quotemark */
  getData(): Array<StoreModel> {
    return [
      {
        id: "912836d6-6fd7-11eb-9439-0242ac130002",
        name: "Galeries Gourmandes - Palais des Congrès",
        description: "Au croisement d'un supermarché traditionnel et une épicerie fine haut de gamme, vous y trouverez un large choix de produits de consommation courante associé aux grands noms et grands classiques de la gastronomie française et internationale.",
        category: "EPICERIE",
        labels: [],
        capacity: 3,
        start: "2021-02-08T13:30:00Z",
        end: "2021-02-08T14:00:00Z"
      },
      {
        id: "a1972c7a-6fd7-11eb-9439-0242ac130002",
        name: "Ladurée - CNIT (Panier Surprise)",
        description: "La Maison Ladurée allie à ses valeurs la lutte contre le gaspillage alimentaire.",
        category: "PATISSERIE",
        labels: [],
        capacity: 0,
        start: "2021-02-09T11:00:00Z",
        end: "2021-02-09T13:00:00Z"
      },
      {
        id: "ab4b4774-6fd7-11eb-9439-0242ac130002",
        name: "Monop' - Neuilly Sur Seine , Neuilly",
        description: "Venez sauver plein de fruits et légumes ! Votre panier surprise sera composé de fruits et légumes moches ou légèrement abîmés.",
        category: "EPICERIE",
        labels: ["BIO", "VEGAN"],
        capacity: 2,
        start: "2021-02-09T11:00:00Z",
        end: "2021-02-09T13:00:00Z"
      },
      {
        id: "b614ca0e-6fd7-11eb-9439-0242ac130002",
        name: "Starbucks - Léonard de Vinci",
        description: "Dans ton panier-surprise tu pourras trouver des muffins, des cookies, des sandwiches et plein d’autres plaisirs gourmands !",
        category: "REPAS",
        labels: [],
        capacity: 5,
        start: "2021-02-09T13:30:00Z",
        end: "2021-02-09T14:00:00Z"
      },
      {
        id: "c19c656c-6fd7-11eb-9439-0242ac130002",
        name: "EXKi - Passage de l'Arche",
        description: "Découvre un bel échantillon des produits frais d'EXKi et régale-toi en goûtant aux salades, sandwichs, gourmandises, tartes salées ou sucrées !",
        category: "REPAS",
        labels: [],
        capacity: 5,
        start: "2021-02-09T11:00:00Z",
        end: "2021-02-09T13:30:00Z"
      },
      {
        id: "c9efb390-6fd7-11eb-9439-0242ac130002",
        name: "Angelina - Palais des Congrès",
        description: "Angelina Paris s’engage dans la lutte contre le gaspillage alimentaire !",
        category: "PATISSERIE",
        labels: ["VEGAN"],
        capacity: 0,
        start: "2021-02-08T16:00:00Z",
        end: "2021-02-08T16:30:00Z"
      },
      {
        id: "d41c772c-6fd7-11eb-9439-0242ac130002",
        name: "Les Jardins De Courbevoie",
        description: "",
        category: "EPICERIE",
        labels: [],
        capacity: 0,
        start: "2021-02-09T16:30:00Z",
        end: "2021-02-09T17:00:00Z",
      },
      {
        id: "dd8e8ffc-6fd7-11eb-9439-0242ac130002",
        name: "Naturalia Vegan - Paris, Rome",
        description: "Venez sauver plein de fruits et légumes bio !",
        category: "EPICERIE",
        labels: ['BIO', 'VEGAN'],
        capacity: 8,
        start: "2021-02-09T11:00:00Z",
        end: "2021-02-09T13:00:00Z"
      }
    ];
  }
}
